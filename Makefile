# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jwong <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/01/22 10:37:17 by jwong             #+#    #+#              #
#    Updated: 2016/09/30 14:35:57 by jwong            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	= fillit

SRC		= main.c 				\
		  ft_open_file.c		\
		  ft_get_tetriminos.c	\
		  ft_check_buffer.c		\
		  ft_get_shape.c		\
		  ft_make_grid.c		\
		  ft_min_square.c		\
		  ft_clear_shape.c		\
		  ft_fill.c				\
		  ft_solve.c			\
		  ft_print_grid.c

CC		= clang
CFLAGS	= -Wall -Werror -Wextra 
OBJ		= $(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
		$(CC) $(CFLAGS) $(OBJ) -o $(NAME)

%.o: %.c
		$(CC) $(CFLAG) $< -c -o $@

clean:
		rm -f $(OBJ)

fclean: clean
		rm -f $(NAME)

re: fclean all
