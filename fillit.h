/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 15:51:54 by jwong             #+#    #+#             */
/*   Updated: 2016/02/04 16:03:47 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef H_FILLIT_H
# define H_FILLIT_H

# define BUFF_SIZE 21

typedef struct	s_tetriminos
{
	char	letter;
	int		shape;
}				t_tetriminos;

typedef struct	s_gridinfo
{
	int		total;
	int		index;
	int		width;
}				t_gridinfo;

int				ft_open_file(const char *file);
int				ft_get_tetriminos(t_tetriminos *shapes, int fd);
int				ft_check_buffer(char *s);
int				ft_get_shape(char *s);
char			*ft_make_grid(int size);
int				ft_min_square(int count);
void			ft_clear_shape(char *grid, char c);
void			ft_print_grid(char *grid);
void			ft_fill(t_gridinfo *info, t_tetriminos *shapes, int i, char *s);
void			ft_solve(t_gridinfo *info, t_tetriminos *shapes, int i,
						char *grid);

#endif
