/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_buffer.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 15:53:53 by jwong             #+#    #+#             */
/*   Updated: 2016/01/27 13:35:38 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int	ft_check_sharps(char *s)
{
	int	sharps;

	sharps = 0;
	while (*s)
	{
		if (*s == '#')
			sharps++;
		s++;
	}
	if (sharps == 4)
		return (1);
	return (-1);
}

static int	ft_check_characters(char *s)
{
	while (*s)
	{
		if (*s != '#' && *s != '.' && *s != '\n')
			return (-1);
		s++;
	}
	return (1);
}

static int	ft_check_newlines(char *s)
{
	if (s[4] == '\n'
			&& s[9] == '\n'
			&& s[14] == '\n'
			&& s[19] == '\n'
			&& (s[20] == '\n' || s[20] == '\0'))
		return (1);
	return (-1);
}

static int	ft_check_dots(char *s)
{
	int	dots;

	dots = 0;
	while (*s)
	{
		if (*s == '.')
			dots++;
		s++;
	}
	if (dots == 12)
		return (1);
	return (-1);
}

int			ft_check_buffer(char *s)
{
	if (ft_check_characters(s) == 1
			&& ft_check_sharps(s) == 1
			&& ft_check_newlines(s) == 1
			&& ft_check_dots(s) == 1)
		return (1);
	return (-1);
}
