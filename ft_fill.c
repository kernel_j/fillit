/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fill.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 15:55:38 by jwong             #+#    #+#             */
/*   Updated: 2016/02/05 14:34:10 by akopera          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	ft_fill19(t_gridinfo *info, t_tetriminos *shapes, int i, char *s)
{
	int wi;
	int tetri;

	wi = (*info).width;
	tetri = (*info).index;
	if (i + wi + 1 < (wi * (wi - 1)))
	{
		if (s[i] == '.')
		{
			if (shapes[tetri].shape == 17 && s[i + 1] == '.' &&
					s[i + wi] == '.' && s[i + (wi + 1)] == '.')
			{
				s[i] = shapes[tetri].letter;
				s[i + 1] = shapes[tetri].letter;
				s[i + wi] = shapes[tetri].letter;
				s[i + wi + 1] = shapes[tetri].letter;
			}
		}
	}
}

void	ft_fill18(t_gridinfo *info, t_tetriminos *shapes, int i, char *s)
{
	int wi;
	int tetri;

	wi = (*info).width;
	tetri = (*info).index;
	if (i + wi + 1 < (wi * (wi - 1)))
	{
		if (s[i] == '.')
		{
			if (shapes[tetri].shape == 16 && s[i + (wi - 1)] == '.' &&
					s[i + wi] == '.' && s[i + (wi + 1)] == '.')
			{
				s[i] = shapes[tetri].letter;
				s[i + wi - 1] = shapes[tetri].letter;
				s[i + wi] = shapes[tetri].letter;
				s[i + wi + 1] = shapes[tetri].letter;
			}
		}
	}
	ft_fill19(info, shapes, i, s);
}

void	ft_fill17(t_gridinfo *info, t_tetriminos *shapes, int i, char *s)
{
	int wi;
	int tetri;

	wi = (*info).width;
	tetri = (*info).index;
	if (i + wi + 1 < (wi * (wi - 1)))
	{
		if (s[i] == '.')
		{
			if (shapes[tetri].shape == 14 && s[i + 1] == '.' &&
					s[i + 2] == '.' && s[i + (wi + 1)] == '.')
			{
				s[i] = shapes[tetri].letter;
				s[i + 1] = shapes[tetri].letter;
				s[i + 2] = shapes[tetri].letter;
				s[i + wi + 1] = shapes[tetri].letter;
			}
		}
	}
	ft_fill18(info, shapes, i, s);
}

void	ft_fill16(t_gridinfo *info, t_tetriminos *shapes, int i, char *s)
{
	int wi;
	int tetri;

	wi = (*info).width;
	tetri = (*info).index;
	if (i + wi < (wi * (wi - 1)))
	{
		if (s[i] == '.')
		{
			if (shapes[tetri].shape == 8 && s[i + (wi - 2)] == '.' &&
					s[i + (wi - 1)] == '.' && s[i + wi] == '.')
			{
				s[i] = shapes[tetri].letter;
				s[i + wi - 2] = shapes[tetri].letter;
				s[i + wi - 1] = shapes[tetri].letter;
				s[i + wi] = shapes[tetri].letter;
			}
		}
	}
	ft_fill17(info, shapes, i, s);
}

void	ft_fill15(t_gridinfo *info, t_tetriminos *shapes, int i, char *s)
{
	int wi;
	int tetri;

	wi = (*info).width;
	tetri = (*info).index;
	if (i + wi < (wi * (wi - 1)))
	{
		if (s[i] == '.')
		{
			if (shapes[tetri].shape == 6 && s[i + 1] == '.' &&
					s[i + 2] == '.' && s[i + wi] == '.')
			{
				s[i] = shapes[tetri].letter;
				s[i + 1] = shapes[tetri].letter;
				s[i + 2] = shapes[tetri].letter;
				s[i + wi] = shapes[tetri].letter;
			}
		}
	}
	ft_fill16(info, shapes, i, s);
}

void	ft_fill14(t_gridinfo *info, t_tetriminos *shapes, int i, char *s)
{
	int wi;
	int tetri;

	wi = (*info).width;
	tetri = (*info).index;
	if (i + wi < (wi * (wi - 1)))
	{
		if (s[i] == '.')
		{
			if (shapes[tetri].shape == 2 && s[i + 1] == '.' &&
					s[i + (wi - 1)] == '.' && s[i + wi] == '.')
			{
				s[i] = shapes[tetri].letter;
				s[i + 1] = shapes[tetri].letter;
				s[i + wi - 1] = shapes[tetri].letter;
				s[i + wi] = shapes[tetri].letter;
			}
		}
	}
	ft_fill15(info, shapes, i, s);
}

void	ft_fill13(t_gridinfo *info, t_tetriminos *shapes, int i, char *s)
{
	int wi;
	int tetri;

	wi = (*info).width;
	tetri = (*info).index;
	if (i + 3 < (wi * (wi - 1)))
	{
		if (shapes[tetri].shape == 19 && s[i] == '.' &&
				s[i + 1] == '.' && s[i + 2] == '.' && s[i + 3] == '.')
		{
			s[i] = shapes[tetri].letter;
			s[i + 1] = shapes[tetri].letter;
			s[i + 2] = shapes[tetri].letter;
			s[i + 3] = shapes[tetri].letter;
		}
	}
	ft_fill14(info, shapes, i, s);
}

void	ft_fill12(t_gridinfo *info, t_tetriminos *shapes, int i, char *s)
{
	int wi;
	int tetri;

	wi = (*info).width;
	tetri = (*info).index;
	if (i + (wi * 2 - 1) < (wi * (wi - 1)))
	{
		if (s[i] == '.')
		{
			if (shapes[tetri].shape == 3 && s[i + (wi - 1)] == '.' &&
					s[i + wi] == '.' && s[i + (wi * 2 - 1)] == '.')
			{
				s[i] = shapes[tetri].letter;
				s[i + wi - 1] = shapes[tetri].letter;
				s[i + wi] = shapes[tetri].letter;
				s[i + wi * 2 - 1] = shapes[tetri].letter;
			}
		}
	}
	ft_fill13(info, shapes, i, s);
}

void	ft_fill11(t_gridinfo *info, t_tetriminos *shapes, int i, char *s)
{
	int wi;
	int tetri;

	wi = (*info).width;
	tetri = (*info).index;
	if (i + (wi + 2) < (wi * (wi - 1)))
	{
		if (s[i] == '.')
		{
			if (shapes[tetri].shape == 12 && s[i + 1] == '.' &&
					s[i + 2] == '.' && s[i + (wi + 2)] == '.')
			{
				s[i] = shapes[tetri].letter;
				s[i + 1] = shapes[tetri].letter;
				s[i + 2] = shapes[tetri].letter;
				s[i + wi + 2] = shapes[tetri].letter;
			}
		}
	}
	ft_fill12(info, shapes, i, s);
}

void	ft_fill10(t_gridinfo *info, t_tetriminos *shapes, int i, char *s)
{
	int wi;
	int tetri;

	wi = (*info).width;
	tetri = (*info).index;
	if (i + (wi + 2) < (wi * (wi - 1)))
	{
		if (s[i] == '.')
		{
			if (shapes[tetri].shape == 10 && s[i + wi] == '.' &&
					s[i + (wi + 1)] == '.' && s[i + (wi + 2)] == '.')
			{
				s[i] = shapes[tetri].letter;
				s[i + wi] = shapes[tetri].letter;
				s[i + wi + 1] = shapes[tetri].letter;
				s[i + wi + 2] = shapes[tetri].letter;
			}
		}
	}
	ft_fill11(info, shapes, i, s);
}

void	ft_fill9(t_gridinfo *info, t_tetriminos *shapes, int i, char *s)
{
	int wi;
	int tetri;

	wi = (*info).width;
	tetri = (*info).index;
	if (i + (wi + 2) < (wi * (wi - 1)))
	{
		if (s[i] == '.')
		{
			if (shapes[tetri].shape == 4 && s[i + 1] == '.' &&
					s[i + (wi + 1)] == '.' && s[i + (wi + 2)] == '.')
			{
				s[i] = shapes[tetri].letter;
				s[i + 1] = shapes[tetri].letter;
				s[i + wi + 1] = shapes[tetri].letter;
				s[i + wi + 2] = shapes[tetri].letter;
			}
		}
	}
	ft_fill10(info, shapes, i, s);
}

void	ft_fill8(t_gridinfo *info, t_tetriminos *shapes, int i, char *s)
{
	int wi;
	int tetri;

	wi = (*info).width;
	tetri = (*info).index;
	if (i + (wi * 2 + 1) < (wi * (wi - 1)))
	{
		if (s[i] == '.')
		{
			if (shapes[tetri].shape == 7 && s[i + 1] == '.' &&
					s[i + (wi + 1)] == '.' && s[i + (wi * 2 + 1)] == '.')
			{
				s[i] = shapes[tetri].letter;
				s[i + 1] = shapes[tetri].letter;
				s[i + wi + 1] = shapes[tetri].letter;
				s[i + wi * 2 + 1] = shapes[tetri].letter;
			}
		}
	}
	ft_fill9(info, shapes, i, s);
}

void	ft_fill7(t_gridinfo *info, t_tetriminos *shapes, int i, char *s)
{
	int wi;
	int tetri;

	wi = (*info).width;
	tetri = (*info).index;
	if (i + (wi * 2 + 1) < (wi * (wi - 1)))
	{
		if (s[i] == '.')
		{
			if (shapes[tetri].shape == 5 && s[i + wi] == '.' &&
					s[i + (wi * 2)] == '.' && s[i + (wi * 2 + 1)] == '.')
			{
				s[i] = shapes[tetri].letter;
				s[i + wi] = shapes[tetri].letter;
				s[i + wi * 2] = shapes[tetri].letter;
				s[i + wi * 2 + 1] = shapes[tetri].letter;
			}
		}
	}
	ft_fill8(info, shapes, i, s);
}

void	ft_fill6(t_gridinfo *info, t_tetriminos *shapes, int i, char *s)
{
	int wi;
	int tetri;

	wi = (*info).width;
	tetri = (*info).index;
	if (i + (wi * 2 + 1) < (wi * (wi - 1)))
	{
		if (s[i] == '.')
		{
			if (shapes[tetri].shape == 1 && s[i + wi] == '.' &&
					s[i + (wi + 1)] == '.' && s[i + (wi * 2 + 1)] == '.')
			{
				s[i] = shapes[tetri].letter;
				s[i + wi] = shapes[tetri].letter;
				s[i + wi + 1] = shapes[tetri].letter;
				s[i + wi * 2 + 1] = shapes[tetri].letter;
			}
		}
	}
	ft_fill7(info, shapes, i, s);
}

void	ft_fill5(t_gridinfo *info, t_tetriminos *shapes, int i, char *s)
{
	int wi;
	int tetri;

	wi = (*info).width;
	tetri = (*info).index;
	if (i + (2 * wi) < (wi * (wi - 1)))
	{
		if (s[i] == '.')
		{
			if (shapes[tetri].shape == 15 && s[i + (wi - 1)] == '.' &&
					s[i + wi] == '.' && s[i + (wi * 2)] == '.')
			{
				s[i] = shapes[tetri].letter;
				s[i + wi - 1] = shapes[tetri].letter;
				s[i + wi] = shapes[tetri].letter;
				s[i + wi * 2] = shapes[tetri].letter;
			}
		}
	}
	ft_fill6(info, shapes, i, s);
}

void	ft_fill4(t_gridinfo *info, t_tetriminos *shapes, int i, char *s)
{
	int wi;
	int tetri;

	wi = (*info).width;
	tetri = (*info).index;
	if (i + (2 * wi) < (wi * (wi - 1)))
	{
		if (s[i] == '.')
		{
			if (shapes[tetri].shape == 13 && s[i + wi] == '.' &&
					s[i + (wi + 1)] == '.' && s[i + (wi * 2)] == '.')
			{
				s[i] = shapes[tetri].letter;
				s[i + wi] = shapes[tetri].letter;
				s[i + wi + 1] = shapes[tetri].letter;
				s[i + wi * 2] = shapes[tetri].letter;
			}
		}
	}
	ft_fill5(info, shapes, i, s);
}

void	ft_fill3(t_gridinfo *info, t_tetriminos *shapes, int i, char *s)
{
	int wi;
	int tetri;

	wi = (*info).width;
	tetri = (*info).index;
	if (i + (2 * wi) < (wi * (wi - 1)))
	{
		if (s[i] == '.')
		{
			if (shapes[tetri].shape == 9 && s[i + wi] == '.' &&
					s[i + (wi * 2 - 1)] == '.' && s[i + (wi * 2)] == '.')
			{
				s[i] = shapes[tetri].letter;
				s[i + wi] = shapes[tetri].letter;
				s[i + wi * 2 - 1] = shapes[tetri].letter;
				s[i + wi * 2] = shapes[tetri].letter;
			}
		}
	}
	ft_fill4(info, shapes, i, s);
}

void	ft_fill2(t_gridinfo *info, t_tetriminos *shapes, int i, char *s)
{
	int wi;
	int tetri;

	wi = (*info).width;
	tetri = (*info).index;
	if (i + (2 * wi) < (wi * (wi - 1)))
	{
		if (s[i] == '.')
		{
			if (shapes[tetri].shape == 11 && s[i + 1] == '.' &&
					s[i + wi] == '.' && s[i + (wi * 2)] == '.')
			{
				s[i] = shapes[tetri].letter;
				s[i + 1] = shapes[tetri].letter;
				s[i + wi] = shapes[tetri].letter;
				s[i + wi * 2] = shapes[tetri].letter;
			}
		}
	}
	ft_fill3(info, shapes, i, s);
}

void	ft_fill(t_gridinfo *info, t_tetriminos *shapes, int i, char *s)
{
	int wi;
	int tetri;

	wi = (*info).width;
	tetri = (*info).index;
	if (i + (3 * wi) < (wi * (wi - 1)))
	{
		if (shapes[tetri].shape == 18 && s[i] == '.' && s[i + wi] == '.' &&
				s[i + (2 * wi)] == '.' && s[i + (3 * wi)] == '.')
		{
			s[i] = shapes[tetri].letter;
			s[i + wi] = shapes[tetri].letter;
			s[i + 2 * wi] = shapes[tetri].letter;
			s[i + 3 * wi] = shapes[tetri].letter;
		}
	}
	ft_fill2(info, shapes, i, s);
}
