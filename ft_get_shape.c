/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_shape.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 15:55:38 by jwong             #+#    #+#             */
/*   Updated: 2016/01/20 15:56:51 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "fillit.h"

static int	ft_shape_check4(char *s, int i)
{
	if (i < 6)
	{
		if (s[i + 5] == '#' && s[i + 10] == '#' && s[i + 15] == '#')
			return (18);
	}
	return (-1);
}

static int	ft_shape_check3(char *s, int i)
{
	if (i < 11)
	{
		if (s[i + 1] == '#' && s[i + 5] == '#' && s[i + 10] == '#')
			return (11);
		if (s[i + 5] == '#' && s[i + 9] == '#' && s[i + 10] == '#')
			return (9);
		if (s[i + 5] == '#' && s[i + 6] == '#' && s[i + 10] == '#')
			return (13);
		if (s[i + 4] == '#' && s[i + 5] == '#' && s[i + 10] == '#')
			return (15);
		if (i < 10)
		{
			if (s[i + 5] == '#' && s[i + 6] == '#' && s[i + 11] == '#')
				return (1);
			if (s[i + 5] == '#' && s[i + 10] == '#' && s[i + 11] == '#')
				return (5);
			if (s[i + 1] == '#' && s[i + 6] == '#' && s[i + 11] == '#')
				return (7);
		}
	}
	return (ft_shape_check4(s, i));
}

static int	ft_shape_check2(char *s, int i)
{
	if (i < 14)
	{
		if (s[i + 1] == '#' && s[i + 6] == '#' && s[i + 7] == '#')
			return (4);
		if (s[i + 5] == '#' && s[i + 6] == '#' && s[i + 7] == '#')
			return (10);
		if (s[i + 1] == '#' && s[i + 2] == '#' && s[i + 7] == '#')
			return (12);
		if (i < 12)
		{
			if (s[i + 4] == '#' && s[i + 5] == '#' && s[i + 9] == '#')
				return (3);
		}
	}
	return (ft_shape_check3(s, i));
}

static int	ft_shape_check(char *s, int i)
{
	if (i < 17)
	{
		if (s[i + 1] == '#' && s[i + 2] == '#' && s[i + 3] == '#')
			return (19);
		if (i < 16)
		{
			if (s[i + 1] == '#' && s[i + 4] == '#' && s[i + 5] == '#')
				return (2);
			if (s[i + 1] == '#' && s[i + 2] == '#' && s[i + 5] == '#')
				return (6);
			if (s[i + 3] == '#' && s[i + 4] == '#' && s[i + 5] == '#')
				return (8);
			if (i < 15)
			{
				if (s[i + 1] == '#' && s[i + 2] == '#' && s[i + 6] == '#')
					return (14);
				if (s[i + 4] == '#' && s[i + 5] == '#' && s[i + 6] == '#')
					return (16);
				if (s[i + 1] == '#' && s[i + 5] == '#' && s[i + 6] == '#')
					return (17);
			}
		}
	}
	return (ft_shape_check2(s, i));
}

int			ft_get_shape(char *s)
{
	int	i;

	i = 0;
	while (s[i] && s[i] != '#')
		i++;
	return (ft_shape_check(s, i));
}
