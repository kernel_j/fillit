/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_tetriminos.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/04 16:01:07 by jwong             #+#    #+#             */
/*   Updated: 2016/02/04 16:01:11 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include "fillit.h"

static char	*ft_get_buffer(int fd)
{
	char	*buffer;
	int		bytes_read;

	buffer = (char *)malloc(sizeof(*buffer) * (BUFF_SIZE + 1));
	if (buffer != NULL)
	{
		bytes_read = read(fd, buffer, BUFF_SIZE);
		if (bytes_read != 0)
			buffer[bytes_read] = '\0';
		else
		{
			free(buffer);
			buffer = NULL;
		}
	}
	return (buffer);
}

static int	ft_store_shapes(t_tetriminos *shapes, char *buffer, int total)
{
	int	num;

	if (ft_check_buffer(buffer) == 1)
	{
		num = ft_get_shape(buffer);
		if (num != -1)
		{
			shapes[total].letter = 'A' + total;
			shapes[total].shape = num;
			return (1);
		}
	}
	return (-1);
}

int			ft_get_tetriminos(t_tetriminos *shapes, int fd)
{
	char	*buffer;
	int		total;

	total = 0;
	buffer = ft_get_buffer(fd);
	while (buffer != NULL && total < 26)
	{
		if (ft_store_shapes(shapes, buffer, total) == 1)
			total++;
		else
		{
			write(1, "error\n", 6);
			return (-1);
		}
		free(buffer);
		buffer = NULL;
		buffer = ft_get_buffer(fd);
	}
	if (buffer != NULL)
	{
		free(buffer);
		write(1, "error\n", 6);
		return (-1);
	}
	return (total);
}
