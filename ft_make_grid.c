/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_make_grid.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 16:57:00 by jwong             #+#    #+#             */
/*   Updated: 2016/01/20 17:30:25 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "fillit.h"

static void	ft_put_newline(char *s, int size)
{
	int	line;
	int	i;

	line = 0;
	size -= 1;
	i = size;
	while (line < size)
	{
		s[i] = '\n';
		i += (size + 1);
		line++;
	}
}

static void	ft_initialize_grid(char *s, int len)
{
	int	i;

	i = 0;
	while (i < len)
	{
		s[i] = '.';
		i++;
	}
	s[i] = '\0';
}

char		*ft_make_grid(int size)
{
	int		len;
	char	*grid;

	size += 1;
	len = (size * (size - 1));
	grid = (char *)malloc(sizeof(*grid) * (len + 1));
	if (grid != NULL)
	{
		ft_initialize_grid(grid, len);
		ft_put_newline(grid, size);
	}
	return (grid);
}
