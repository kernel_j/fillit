/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_solve.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/04 16:00:14 by jwong             #+#    #+#             */
/*   Updated: 2016/02/04 16:00:23 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "fillit.h"

static int	ft_place_shape(t_gridinfo *info, t_tetriminos *shapes,
							int i, char *grid)
{
	while (grid[i])
	{
		ft_fill(info, shapes, i, grid);
		if (grid[i] == shapes[(*info).index].letter)
		{
			(*info).index++;
			return (1);
		}
		i++;
	}
	return (-1);
}

static int	ft_get_next_position(char c, char *grid)
{
	int	i;

	i = 0;
	while (grid[i] && grid[i] != c)
		i++;
	return (++i);
}

static int	ft_resolve(t_tetriminos *shapes, t_gridinfo *info,
						int i, char *grid)
{
	int	pos;

	if (ft_place_shape(info, shapes, i, grid) == -1)
		return (-1);
	else
	{
		if ((*info).index < (*info).total)
		{
			if (ft_resolve(shapes, info, 0, grid) == -1)
			{
				(*info).index--;
				pos = ft_get_next_position(shapes[(*info).index].letter, grid);
				ft_clear_shape(grid, shapes[(*info).index].letter);
				if (ft_resolve(shapes, info, pos, grid) == -1)
					return (-1);
			}
		}
		else
		{
			(*info).index--;
			if (ft_place_shape(info, shapes, 0, grid) == -1)
				return (-1);
		}
	}
	return (1);
}

void		ft_solve(t_gridinfo *info, t_tetriminos *shapes, int i, char *grid)
{
	int	result;

	while ((*info).index < (*info).total)
	{
		result = ft_resolve(shapes, info, i, grid);
		if (result == -1)
		{
			free(grid);
			(*info).width += 1;
			grid = ft_make_grid(((*info).width - 1));
			(*info).index = 0;
			i = 0;
		}
	}
	ft_print_grid(grid);
}
