/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 15:57:41 by jwong             #+#    #+#             */
/*   Updated: 2016/02/05 13:39:02 by akopera          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

#include <unistd.h>
#include <stdlib.h>
#include "fillit.h"

int	main(int argc, char **argv)
{
	char			*grid;
	int				fd;
	int				total;
	t_gridinfo		info;
	t_tetriminos	shapes[27];

	if (argc != 2)
		write(1, "error\n", 6);
	else
	{
		fd = ft_open_file(argv[1]);
		if (fd != -1)
		{
			total = ft_get_tetriminos(shapes, fd);
			if (total == -1)
				exit(0);
		}
		if (close(fd) != -1)
		{
			info.total = total;
			info.index = 0;
			info.width = ft_min_square(total) + 1;
			grid = ft_make_grid(info.width - 1);
			ft_solve(&info, shapes, 0, grid);
		}
	}
	return (0);
}
